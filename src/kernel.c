/**
 * @file
 * @ingroup kernel_code
 * @author Erwin Meza <emezav@gmail.com>
 * @copyright GNU Public License.
 *
 * @brief Código de inicialización del kernel en C
 *
 * Este codigo recibe el control de start.S y continúa con la ejecución.
*/
#include <asm.h>

#include <console.h>

#include <irq.h>
#include <paging.h>
#include <pci.h>
#include <pm.h>
#include <physmem.h>
#include <stdlib.h>
#include <string.h>

void timer_handler(interrupt_state * state);
void keyboard_handler(interrupt_state * state);
void imprimir_caracteres();

//ticks es la variable global que acumulara el numero de interrupciones del timer
unsigned int ticks = 0;


//el mapa de teclado espaniol
char kb_map[]= {0,0, '1','2','3','4','5','6','7','8','9','0','-','=','0','\t','Q','W','E','R','T','Y','U','I','O','P','[',']','\n','0','A','S','D','F','G','H','J','K','L',';','0','0','0','0','Z','X','C','V','B','N','M',',','.','/','0','*','0','0','0','0',};
//el buffer en donde se guardaran los caracteres tecleados por el ususario
char bk_buff [4096];
//numero de teclas que ha precionado el usuario
unsigned int kb_pos= 0;
/*funcion principal*/
void cmain(){

    /* Inicializar y limpiar la consola console.c*/
    console_clear();

     /* Inicializar la estructura para gestionar la memoria física. physmem.c*/
    setup_physical_memory();

    /* Configura la IDT y el PIC.interrupt.c */
    setup_interrupts();

    /* Completa la configuración de la memoria virtual. paging.c*/
    setup_paging();

    console_printf("Kernel started!\n");

    /* Detecta los dispositivos PCI conectados. pci.c */
    pci_detect();

    //pci_list();
   /*Creamos un manejador */
   install_irq_handler(0, timer_handler);
   install_irq_handler(1, keyboard_handler);


}

/*funcion que maneja una interrupcion producida por el timer */
void timer_handler(interrupt_state * state){
	/**Se define la direccion virtual del inicio de la memoria de video*/
	unsigned short * ptr = (unsigned short *) VIDEO_START_ADDR;
	/*Acumula el numero de ticks*/
	ticks ++;
	char i;
	char buf[10];
	/*funcion para limpiar el buffer*/
	memset(buf,0,10);
	/*//Se convierte un integer a un ascii y se guarda en base 10 en el buf*/
	itoa(ticks,buf,10);

	for(i=0; buf[i]!= 0; i++){
		/*Se envia a la memoria de video un byte desplazado 8 bits y se
		construye un short: para posteriormente hacer "or" con otro byte (buf[i])*/
		*ptr = ((0x07f<<8) | buf[i]);
		ptr++;//Incrementa una posicion para leer un nuevo caracter
	}

}
/*funcion que maneja una interrupcion proveniente del teclado*/
void keyboard_handler(interrupt_state * state){

	unsigned short kb_status_port = 0x64; //puerto de estado del controlador del teclado
	unsigned short kb_output_port = 0x60;//puerto de datos  del controlador del teclado

	unsigned char data;
	unsigned char status;
	char caracter;

	status =  inb(kb_status_port);
	if(status & 1){

		data = inb (kb_output_port);


		if(!(data & 0x80)){
			caracter = kb_map[data];

			if(caracter){
				if((data & 0x000000FF) == 0x1c){
					imprimir_caracteres();
					memset(bk_buff,0,kb_pos);
					kb_pos = 0;

				}else{
          console_printf("\ndata 0x%x ASCII = %c ",(data & 0x000000FF), caracter);
  				bk_buff [kb_pos]= caracter;
  				kb_pos =kb_pos +1;
        }
        if(kb_pos >3 && strcmp(bk_buff,"HALT") == 0){
    				console_printf("\nEntrando en la espera Activa");
    				while(1){}
				}
			}

		}


	}

}


 void imprimir_caracteres(){
   console_printf("\nBuffer: ");
   if(kb_pos == 0){
      console_printf("is empty");
      return;
    }
 	  int i;
	 for(i=0; i<kb_pos;i++){
 		console_printf("%c",bk_buff[i]);
	 }
     console_printf("\n");
}
